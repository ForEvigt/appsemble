import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  'http-basic': 'HTTP basic authentication',
  'client-certificate': 'Client certificate authentication',
  'client-credentials': 'OAuth client credentials',
  cookie: 'Cookie authentication',
  'custom-header': 'Header based authentication',
  'query-parameter': 'Query parameter',
});
