import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  queryLabel: 'Query parameter',
  queryHelp: 'The query parameter for query parameter authentication',
  secretLabel: 'Secret',
  secretHelp: 'The secret for query parameter authentication',
});
