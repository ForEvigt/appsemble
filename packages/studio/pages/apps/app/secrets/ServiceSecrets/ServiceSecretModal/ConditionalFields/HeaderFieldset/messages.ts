import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  headerLabel: 'Header',
  headerHelp: 'The header for header based authentication',
  secretLabel: 'Secret',
  secretHelp: 'The secret for header based authentication',
});
