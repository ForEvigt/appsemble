import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  usernameLabel: 'Username',
  usernameHelp: 'The username for HTTP basic authentication',
  passwordLabel: 'Password',
  passwordHelp: 'The password for HTTP basic authentication',
});
