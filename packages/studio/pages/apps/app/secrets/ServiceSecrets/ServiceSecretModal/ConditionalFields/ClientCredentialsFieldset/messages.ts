import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  clientIdLabel: 'Client id',
  clientIdHelp: 'The client id used to authenticate for the OAuth client credentials flow',
  clientSecretLabel: 'Client Secret',
  clientSecretHelp: 'The client secret used to authenticate for the OAuth client credentials flow',
  tokenUrlLabel: 'Token URL',
  tokenUrlHelp: 'The URL to retrieve the token form',
});
