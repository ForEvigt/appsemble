import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  cookieLabel: 'Cookie',
  cookieHelp: 'The cookie for cookie authentication',
  secretLabel: 'Secret',
  secretHelp: 'The secret for cookie authentication',
});
