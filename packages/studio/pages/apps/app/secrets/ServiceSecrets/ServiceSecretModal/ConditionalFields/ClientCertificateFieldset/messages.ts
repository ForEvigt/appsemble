import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  certificateLabel: 'Certificate',
  certificateHelp: 'The public certificate for client certificate authentication in PEM format',
  privateKeyLabel: 'Private key',
  privateKeyHelp: 'The private key for client certificate authentication in PEM format',
});
