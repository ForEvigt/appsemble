import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  title: 'Service',
  error: 'There was a problem loading the service secrets.',
  loading: 'Loading service secrets …',
  placeholder: 'example.com',
  noSecrets: 'No service secrets have been configured yet.',
  addNew: 'Add new secret',
});
