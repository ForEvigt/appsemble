import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  error: 'Something went wrong when trying to load the apps.',
  loading: 'Loading apps…',
  emptyApps: 'There are currently no apps available.',
  emptyUserApps: 'There are currently no user apps available.',
  noApps: 'There are no apps that match your search parameters.',
});
