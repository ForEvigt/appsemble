/** @type {import('jest').Config} */
export default {
  modulePathIgnorePatterns: ['./studio', './app'],
  passWithNoTests: true,
};
